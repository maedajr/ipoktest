<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DateTime;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function listar(){

        ini_set('memory_limit','-1');

        // $results = \DB::table('employees')
        // ->select('first_name','gender')
        // ->orderBy('first_name', 'ASC')
        // ->get();

        $resultados = \DB::table('employees')
        ->select('emp_no','first_name','last_name','gender')
        ->orderBy('first_name', 'ASC')
        ->paginate(50);
        // dd(json_encode($resultado));

        // dd(response()->json($result));
        return view('initial', compact('resultados'));
    }

    public function viewemployee($id){
        ini_set('memory_limit','-1');
        $employee = \App\Employee::find($id);
        $date = DB::table('salaries')->select('salaries.to_date')->where('salaries.emp_no','=', $id)->get();
        // dd($date);
        $salario = DB::table('salaries')->select('salaries.emp_no')->join('employees', 'employees.emp_no', '=','salaries.emp_no')->where('salaries.emp_no','=',$id)->get();
        return view('graficosalario', compact('employee','id', 'salario', 'date'))->render();
    }

    public function gerargrafico(){

    }
}
