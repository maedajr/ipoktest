@extends('adminlte::page')

@section('title', 'IPOK')

@section('content_header')
    <h1>Dashboard</h1>
@stop
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@stop
@section('content')
    <p>GRAFICOOOOOOO</p>
    @php 
    ini_set('memory_limit','-1');
    echo $date;
    @endphp
    <canvas class="bar-chart"></canvas>

    @section('js')
    <script> var ctx=document.getElementsByClassName("bar-chart");

    var myBarChart = new Chart (ctx,{
        type:'bar',
        data: {
            labels :['Jan','Fev','Mar','Abr','Mai','Jun','Jul'],
            datasets:[{
                label:"Variacao salarial",
                data: [5,8,4,2,6,9,6,3,2,4,7,9],
                borderWidth:6,
                borderColor: 'rgba(77,166,253,0.85)',
                backgroundColor: 'transparent',
            }]
        }
    })

    </script>
    @stop
@stop