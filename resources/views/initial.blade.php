@extends('adminlte::page')

@section('title', 'IPOK')

@section('content_header')
    <h1>Dashboard</h1>
@stop
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@stop
@section('content')
    <p>Um gráfico aqui</p>
    
<table class="table table-striped">
    <thead>
      <tr>
        <th>Nome</th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th colspan="2">Ação</th>
      </tr>
    </thead>
    <tbody>
      
    @foreach ($resultados as $resultado)
      <tr>
        <td>{{$resultado->first_name." ". $resultado->last_name}}</td> 
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><a href="{{action('HomeController@viewemployee', [$resultado->emp_no])}}" class="btn btn-warning">Visualizar gráfico</a></td>
      </tr>
      @endforeach
    </tbody>
  </table>

{{ $resultados->links() }}




@stop